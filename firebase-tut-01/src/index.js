import { initializeApp } from "firebase/app";
import {
  addDoc,
  collection,
  deleteDoc,
  doc,
  getFirestore,
  onSnapshot,
  orderBy,
  query,
  serverTimestamp,
  updateDoc,
} from "firebase/firestore";

import { createUserWithEmailAndPassword, getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyDoWBQ6jr6DOxHeDWz9IHnOMBSAQrZoLRs",
  authDomain: "hatuna-sot.firebaseapp.com",
  projectId: "hatuna-sot",
  storageBucket: "hatuna-sot.appspot.com",
  messagingSenderId: "312961668960",
  appId: "1:312961668960:web:6dd3b8b64ad6f8c10d7005",
  measurementId: "G-WHS4E0WTZK",
};

initializeApp(firebaseConfig);

// init services
const db = getFirestore(); //db connection
const auth = getAuth(); //authentication service

//collection reference
const colRef = collection(db, "books");

//queries
const q = query(colRef, orderBy("createdAt"));

// real time collection data
// Quando ha mudancas na database isto chama a callback function
// Ou seja quando ha um novo snapshot ele executa a callback function
onSnapshot(q, (snapshot) => {
  let books = [];

  snapshot.docs.forEach((doc) => {
    books.push({
      ...doc.data(),
      id: doc.id,
    });
  });
  console.log(books);
});

// adding docs
const addBookForm = document.querySelector(".add");
addBookForm.addEventListener("submit", (e) => {
  e.preventDefault();

  addDoc(colRef, {
    title: addBookForm.title.value,
    author: addBookForm.author.value,
    createdAt: serverTimestamp(),
  }).then(() => {
    addBookForm.reset();
  });
});

// deleting docs
const deleteBookForm = document.querySelector(".delete");
deleteBookForm.addEventListener("submit", (e) => {
  e.preventDefault();

  const docRef = doc(db, "books", deleteBookForm.id.value);

  deleteDoc(docRef).then(() => {
    deleteBookForm.reset();
  });
});

// getting a single document in realtime
const docRef = doc(db, "books", "6oBwtSlOEe0wdJDtcVs7");
onSnapshot(docRef, (doc) => {
  console.log(doc.data(), doc.id);
});

// updating a document
const updateForm = document.querySelector(".update");
updateForm.addEventListener("submit", (e) => {
  e.preventDefault();

  let docRef = doc(db, "books", updateForm.id.value);

  updateDoc(docRef, {
    title: "updated title",
  }).then(() => {
    updateForm.reset();
  });
});

// signing users up
const signupForm = document.querySelector(".signup");
signupForm.addEventListener("submit", (e) => {
  e.preventDefault();

  const email = signupForm.email.value;
  const password = signupForm.password.value;

  createUserWithEmailAndPassword(auth, email, password)
    .then((credentials) => {
      console.log("User Created: ", credentials.user);
      signupForm.reset();
    })
    .catch((err) => {
      console.log(err);
    });
});
